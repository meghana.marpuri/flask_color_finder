from flask import Blueprint, request

from scripts.handler.color_finder_handler import unique_colors

color_finder = Blueprint('color_finder_blueprint', __name__)


@color_finder.route('/', methods=['POST'])
def post_data():
    content = request.get_json()
    data = unique_colors(content)
    return data
