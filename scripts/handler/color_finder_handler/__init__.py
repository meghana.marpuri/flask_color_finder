colors = []


def unique_colors(data):
    if not isinstance(data, str):
        for k, val in data.items():
            if isinstance(val, dict):
                unique_colors(val)
            elif hasattr(val, '__iter__') and not isinstance(val, str):
                for dictitem in val:
                    unique_colors(dictitem)
            elif isinstance(val, str):
                if k == "color":
                    colors.append(val)
                else:
                    if k == "color":
                        colors.append(val)
    return str(set(colors))
